(function(root, isShowSolution) {
  function addSolutionStylesheets() {
    function addStylesheet(href) {
      var link = document.createElement('link');
      link.rel = 'stylesheet';
      link.href = href;
      document.head.appendChild(link);
    }

    /**
     * Add section stylesheets here
     */
    addStylesheet('solution-horizontally.css');
    addStylesheet('solution-vertically.css');
    addStylesheet('solution-middle.css');
    addStylesheet('solution-horizontal-line.css');
    addStylesheet('solution-vertical-line.css');
    addStylesheet('solution-animate.css');
  }

  function initMenuListener() {
    function createMenuListener(tagName) {
      return function() {
        var sections = root.querySelectorAll('.section');
        sections.forEach(function(section) {
          section.classList.remove('section-visible');
        });

        var element = root.querySelector(tagName);
        if (element) {
          element.classList.add('section-visible');
        }
      }
    }

    /**
     * Add menu event listeners here
     */
    var menuCenterHorizontally = root.querySelector('.menu-center-horizontally')
    if (menuCenterHorizontally) {
      menuCenterHorizontally.addEventListener('click', createMenuListener('.section-center-horizontally'));
    }
    var menuCenterVertically = root.querySelector('.menu-center-vertically');
    if (menuCenterVertically) {
      menuCenterVertically.addEventListener('click', createMenuListener('.section-center-vertically'));
    }
    var menuCenterMiddle = root.querySelector('.menu-center-middle');
    if (menuCenterMiddle) {
      menuCenterMiddle.addEventListener('click', createMenuListener('.section-center-middle'));
    }
    var menuHorizontalLine = root.querySelector('.menu-horizontal-line');
    if (menuHorizontalLine) {
      menuHorizontalLine.addEventListener('click', createMenuListener('.section-horizontal-line'));
    }
    var menuVerticalLine = root.querySelector('.menu-vertical-line');
    if (menuVerticalLine) {
      menuVerticalLine.addEventListener('click', createMenuListener('.section-vertical-line'));
    }
    var menuAnimate = root.querySelector('.menu-animate');
    if (menuAnimate) {
      menuAnimate.addEventListener('click', createMenuListener('.section-animate'));
    }
  }

  window.addEventListener('load', function() {
    if (isShowSolution) {
      addSolutionStylesheets();
    }

    initMenuListener();
  });
})(document, true /* Activate / Deactivate solutions */);
