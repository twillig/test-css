(function(root) {
  window.addEventListener('load', function() {
    var box = root.querySelector('.section-animate .box');

    var buttonCircle = root.querySelector('.section-animate .button-to-circle');
    if (buttonCircle) {
      buttonCircle.addEventListener('click', function (event) {
        if (box) {
          box.classList.add('box-circle');
        }
      });
    }

    var buttonBox = root.querySelector('.section-animate .button-to-box');
    if (buttonBox) {
      buttonBox.addEventListener('click', function (event) {
        if (box) {
          box.classList.remove('box-circle');
        }
      });
    }
  });
})(document);
